package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.DetailsMovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

@Component
public class MovieDetailsMapper {

    public DetailsMovieDto mapToDto(Movie movie) {
        DetailsMovieDto detailsMovieDto = new DetailsMovieDto.Builder().title(movie.getTitle()).videoId(movie.getVideoId()).image(movie.getImage()).year(movie.getYear()).build();

        return detailsMovieDto;
    }

}
