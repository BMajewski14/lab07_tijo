package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.domain.entity.Movie;
import pl.edu.pwsztar.domain.repository.Converter;

import java.util.ArrayList;
import java.util.List;

@Component
public class MovieListMapper implements Converter<MovieDto,Movie> {

    public List<MovieDto> mapToDto(List<Movie> movies) {
        List<MovieDto> moviesDto = new ArrayList<>();

        for(Movie movie: movies) {
            MovieDto movieDto = new MovieDto.Builder().movieId(movie.getMovieId()).title(movie.getTitle()).image(movie.getImage()).year(movie.getYear()).build();

            moviesDto.add(movieDto);
        }

        return moviesDto;
    }

    @Override
    public MovieDto convert(Movie from){
        return new MovieDto();
    }
}
