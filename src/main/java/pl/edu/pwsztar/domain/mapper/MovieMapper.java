package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.domain.entity.Movie;
import pl.edu.pwsztar.domain.repository.Converter;

@Component
public class MovieMapper implements Converter<Movie, MovieDto> {

    public Movie mapToEntity(CreateMovieDto createMovieDto) {
        Movie movie = new Movie.Builder().image(createMovieDto.getImage()).title(createMovieDto.getTitle()).year(createMovieDto.getYear()).videoId(createMovieDto.getVideoId()).build();

        return movie;
    }

    @Override
    public Movie convert(MovieDto from){
        return new Movie();
    }
}
