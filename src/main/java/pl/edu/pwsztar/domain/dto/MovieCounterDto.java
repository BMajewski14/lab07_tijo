package pl.edu.pwsztar.domain.dto;

public class MovieCounterDto {

    private long counter;

    public long getCounter() {
        return counter;
    }

    private MovieCounterDto(Builder builder){
        counter = builder.counter;
    }

    public static final class Builder{
        private long counter;

        public Builder(){

        }

        public Builder counter(long counter){
            this.counter = counter;
            return this;
        }

        public MovieCounterDto build(){
            return new MovieCounterDto(this);
        }

    }

    @Override
    public String toString() {
        return "MovieCounterDto{" +
                "counter=" + counter +
                '}';
    }
}
