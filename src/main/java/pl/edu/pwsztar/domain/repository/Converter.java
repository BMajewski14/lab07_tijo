package pl.edu.pwsztar.domain.repository;

@FunctionalInterface
public interface Converter<T , F> {
    T convert(F from);
}
